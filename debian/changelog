gtg-trace (0.2-3-4) UNRELEASED; urgency=medium

  [ Samuel Thibault ]
  * Disable binding on the smoke test too.
  * patches/git-make-shuffle: Fix make --shuffle build.

  [ Graham Inggs ]
  * rules: Handle nocheck build option (Closes: Bug#1091443)

 -- Samuel Thibault <sthibault@debian.org>  Tue, 29 Oct 2024 20:13:02 +0100

gtg-trace (0.2-3-3) unstable; urgency=medium

  * control: Update upstream URL.
  * test-mpi.c, rules: Add MPI smoketest
  * patches/check: Disable binding (Closes: Bug#1086273)

 -- Samuel Thibault <sthibault@debian.org>  Tue, 29 Oct 2024 20:00:28 +0100

gtg-trace (0.2-3-2) unstable; urgency=medium

  * clean: Clean generated files (Closes: Bug#1044886)
  * patches/implicit: Fix build with qa=+bug-implicit-func (Closes: #1066332).

 -- Samuel Thibault <sthibault@debian.org>  Wed, 13 Mar 2024 12:49:04 +0100

gtg-trace (0.2-3-1) unstable; urgency=medium

  [ Samuel Thibault ]
  * New upstream release
    - patches/clang: Upstreamed.
    - rules: Enable building doc.
    - docs: Drop gtg_refmanual.pdf.
  * control: Set Rules-Requires-Root to no.
  * watch: Update to new home.
  * gbp.conf: Add filter to repack upstream tarball.
  * patches/check: Add check target.
  * patches/debug: Add some debugging information.
  * patches/disable-mpi-otf: Disable mpi + otf, which is currently broken.
  * patches/fix: Fix timestamp ordering.
  * patches/warnings: Fix test build warnings.
  * patches/no-stderr: Avoid printing on stderr.
  * patches/cppflags: Include CPPFLAGS in test compilation.
  * tests/control: Add autopkgtest.
  * rules: Run check.

  [ Debian Janitor ]
  * Update watch file format version to 4.
  * Update standards version to 4.5.1, no changes needed.

 -- Samuel Thibault <sthibault@debian.org>  Mon, 07 Aug 2023 18:08:28 +0200

gtg-trace (0.2-2+dfsg-7) unstable; urgency=medium

  [ Samuel Thibault ]
  * control: Add zlib1g-dev build-dep, add libgtg-tools package.
  * patches/clang: Fix build with clang.
  * Bump debhelper from 10 to 12.
    - not-installed: Note that we don't install .la files, and we install the
    doc in the proper place by hand.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from deprecated 9 to 10.
  * Drop unnecessary dependency on dh-autoreconf.
  * Update standards version to 4.5.0, no changes needed.
  * Remove Section on libgtg0 that duplicates source.

 -- Samuel Thibault <sthibault@debian.org>  Sun, 01 Nov 2020 00:40:06 +0100

gtg-trace (0.2-2+dfsg-6) unstable; urgency=medium

  * control: Bump Standards-Version to 4.4.0 (no changes).
  * control: Replace autotools-dev dep with debhelper (>= 9.20160114) dep.
  * rules: Drop --with autotools_dev.
  * watch: Generalize pattern.
  * copyright: Add missing licence name.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 07 Dec 2019 13:27:48 +0100

gtg-trace (0.2-2+dfsg-5) unstable; urgency=medium

  * patches/doc-nopdf: Disable building pdf documentation
    (Closes: Bug#921768).

 -- Samuel Thibault <sthibault@debian.org>  Sat, 09 Feb 2019 00:30:44 +0100

gtg-trace (0.2-2+dfsg-4) unstable; urgency=medium

  [ Samuel Thibault ]
  * control: Bump Standards-Version to 4.2.0 (no changes).
  * control: Make libgtg-dev Multi-Arch: same.

  [ Ondřej Nový ]
  * d/copyright: Change Format URL to correct one
  * d/control: Set Vcs-* to salsa.debian.org

 -- Samuel Thibault <sthibault@debian.org>  Tue, 01 Jan 2019 17:14:30 +0100

gtg-trace (0.2-2+dfsg-3) unstable; urgency=medium

  * Use canonical anonscm vcs URL.
  * control: Migrate priority to optional.
  * control: Bump Standards-Version to 4.1.4.

 -- Samuel Thibault <sthibault@debian.org>  Sat, 28 Apr 2018 17:24:28 +0200

gtg-trace (0.2-2+dfsg-2) unstable; urgency=medium

  * watch: Generalize URL.
  * Bump Standards-Version to 3.9.8 (no changes).
  * compat: Bump to 9.
  * rules: Clear.
  * control: Drop hardening-wrapper dependency (Closes: Bug#836629).

 -- Samuel Thibault <sthibault@debian.org>  Sun, 04 Sep 2016 19:27:18 +0200

gtg-trace (0.2-2+dfsg-1) unstable; urgency=low

  * New upstream release (Closes: #748973)
  * Fix watch mangling.
  * Bump Standards-Version to 3.9.5 (no changes).

 -- Samuel Thibault <sthibault@debian.org>  Tue, 10 Jun 2014 11:30:51 +0200

gtg-trace (0.2+dfsg-1) unstable; urgency=low

  * Initial release (Closes: #668218)

 -- Samuel Thibault <sthibault@debian.org>  Fri, 20 Apr 2012 18:02:31 +0200
